// npm init -y
// npm install express@4.18.2
// npm install mongoose@7.0.0
// npm install cors@2.8.5
// npm install bcrypt@5.1.0
// npm install jsonwebtoken@9.0.0

// setup dependencies initializing
const express= require("express");
const mongoose= require("mongoose");
const port= 4000;

const cors= require("cors");

const app= express();
app.use(cors(/*{origin: "url" or " * "} - also see methods / credentials*/)); //CORS = Cross-Origin Resources Sharing
app.use(express.json());
app.use(express.urlencoded({extended: true}))

// setup main routes
const userRoutes= require("./routes/userRoutes")
app.use("/users", userRoutes)

const courseRoutes= require("./routes/courseRoutes")
app.use("/courses", courseRoutes)



// connect to our database MongoDB
mongoose.connect("mongodb+srv://tejuco-voren:Cheersbr021@zuitt-bootcamp.muegp8u.mongodb.net/s37-s41?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})
mongoose.connection.once('open', ()=> console.log('Now connected to MongoDB Atlas'))








// listen port localhost
app.listen(port, ()=> console.log(`API is now online on port ${port}`))